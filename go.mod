module gitlab.com/work-projects7/go-netbox-updater

go 1.18

require (
	github.com/vmware/govmomi v0.29.0
	gitlab.com/work-projects7/go-netbox v1.2.3
	gitlab.com/work-projects7/go-vscanner v1.2.0
	gopkg.in/yaml.v2 v2.4.0
)

require github.com/google/uuid v1.3.0 // indirect

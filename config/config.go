package config

import "gitlab.com/work-projects7/go-netbox/client"

type Config struct {
	ApiToken           string                      `yaml:"api_token"`
	ApiAddress         string                      `yaml:"api_address"`
	VcenterInfo        map[string]VCenter          `yaml:"vCenters"`
	LogPath            string                      `yaml:"logpath"`
	DefaultType        client.Reference            `yaml:"vmwaretype"`
	DefaultGroup       client.Reference            `yaml:"netbox_cluster_group"`
	Tenant             client.Reference            `yaml:"tenant"`
	Sites              map[string]client.Reference `yaml:"sites"`
	VCenterCustomField string                      `yaml:"vCenterCustomFieldKey"`
	VMRole             client.Reference            `yaml:"vm_role"`
}

// type Reference struct {
// 	Name string  `yaml:"name"`
// 	Slug string  `yaml:"slug"`
// 	ID   float64 `yaml:"id"`
// }

type VCenter struct {
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

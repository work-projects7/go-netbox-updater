package netbox_test

import (
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/find"
	"github.com/vmware/govmomi/simulator"
	"github.com/vmware/govmomi/view"
	"github.com/vmware/govmomi/vim25/types"
	"gitlab.com/work-projects7/go-netbox-updater/config"
	"gitlab.com/work-projects7/go-netbox-updater/netbox"
	"gitlab.com/work-projects7/go-netbox/client"
	govscanner "gitlab.com/work-projects7/go-vscanner"
)

var JSONClusterDataArr client.NetboxJSONData
var JSONVMDataArr client.NetboxJSONData
var JSONDeviceDataArr client.NetboxJSONData

func TestNetboxData(t *testing.T) {
	ctx := context.Background()

	govmomiClient, ts, err := simVcenter(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer ts.Close()

	Client := govscanner.NewVMWareClient(ctx, "test", govmomiClient, view.NewManager(govmomiClient.Client))

	// testNetboxClient := fakeNetbox()

	testNetboxClient, err := setupFakeNetbox()
	if err != nil {
		t.Fatalf("Error setting up fake netbox environment: %v\n", err)
	}

	var configDataTest config.Config

	// emptyClusterMap := map[string]client.Cluster{}
	emptyDeleteClusterMap := map[string]client.Cluster{}
	missingHostMap := map[string][]string{}
	// emptyVMMap := map[string]client.VirtualMachine{}
	// emptyDeviceMap := map[string]client.Device{}
	// testDeleteDeviceList := []client.Device{}

	// testing building the data maps
	clusterMap, deleteClusterMap, err := netbox.MakeNetboxClusterMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a clusterMap from new netbox data: %v\n", err)
	}
	if len(clusterMap) <= 0 {
		t.Fatalf("Incorrect size for clusterMap, expect <0 received %v\n", len(clusterMap))
	}

	deviceMap, err := netbox.MakeNetboxDeviceMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a deviceMap from new netbox data: %v\n", err)
	}
	if len(deviceMap) <= 0 {
		t.Fatalf("Incorrect size for deviceMap, expect <0 received %v\n", len(deviceMap))
	}
	vmMap, err := netbox.MakeNetboxVirtualMachineMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a vmMap from new netbox data: %v\n", err)
	}
	if len(vmMap) <= 0 {
		t.Fatalf("Incorrect size for vmMap, expect <0 received %v\n", len(vmMap))
	}

	// setting variables for future tests
	oldClusterLen := len(clusterMap)
	oldVMLen := len(vmMap)

	// testing creating new clusters and virtual machines
	err = netbox.UpdateNetboxClusters(Client.VcenterName, &clusterMap, emptyDeleteClusterMap, Client, testNetboxClient, configDataTest)
	if err != nil {
		t.Fatalf("Error creating netbox clusters: %v\n", err)
	}
	_, err = netbox.UpdateNetboxVirtualMachines(vmMap, clusterMap, deviceMap, missingHostMap, Client, testNetboxClient, configDataTest) // breaks device map
	if err != nil {
		t.Fatalf("Error creating netbox VMs: %v\n", err)
	}

	// err = netbox.UpdateNetboxDeviceClusters(deviceMap, clusterMap, emptyDeleteClusterMap, testDeleteDeviceList, Client, testNetboxClient)
	// if err != nil {
	// 	t.Fatalf("Error updating netbox device clusters: %v\n", err)
	// }

	// check to see the creation reflected in updated data maps
	vmMap, err = netbox.MakeNetboxVirtualMachineMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a vmMap from new netbox data: %v\n", err)
	}
	if oldVMLen >= len(vmMap) {
		t.Fatalf("Failed to update vmMap with new data")
	}
	if oldClusterLen >= len(clusterMap) {
		t.Fatalf("Failed to update clusterMap with new data")
	}
	preUpdatedVMLen := len(vmMap)

	// testing update functions
	err = netbox.UpdateNetboxClusters(Client.VcenterName, &clusterMap, deleteClusterMap, Client, testNetboxClient, configDataTest)
	if err != nil {
		t.Fatalf("Error creating netbox clusters: %v\n", err)
	}
	_, err = netbox.UpdateNetboxVirtualMachines(vmMap, clusterMap, deviceMap, missingHostMap, Client, testNetboxClient, configDataTest)
	if err != nil {
		t.Fatalf("Error creating netbox VMs: %v\n", err)
	}

	// check to see if update is reflected in data maps
	updatedVMList := []client.VirtualMachine{}
	for _, vm := range vmMap {
		if vm.Name != "" {
			updatedVMList = append(updatedVMList, vm)
		}
	}
	updatedVMLen := len(updatedVMList)
	updatedClusterLen := len(clusterMap)
	if updatedVMLen != 1 {
		t.Fatalf("Error updating VMs")
	}
	if len(deleteClusterMap) != 1 {
		t.Fatalf("Error updating clusters")
	}

	deleteClusterList := []client.Cluster{}
	deleteVMList := []client.VirtualMachine{}
	for _, value := range deleteClusterMap {
		deleteClusterList = append(deleteClusterList, value)
	}
	for _, value := range vmMap {
		deleteVMList = append(deleteVMList, value)
	}
	// testing delete functions
	err = testNetboxClient.DeleteClusters(deleteClusterList)
	if err != nil {
		t.Fatalf("Error deleting clusters: %v\n", err)
	}
	err = testNetboxClient.DeleteVirtualMachines(deleteVMList)
	if err != nil {
		t.Fatalf("Error deleting vms: %v\n", err)
	}
	// checking delete function by updating data maps
	vmMap, err = netbox.MakeNetboxVirtualMachineMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a vmMap from new netbox data: %v\n", err)
	}
	clusterMap, _, err = netbox.MakeNetboxClusterMap(testNetboxClient)
	if err != nil {
		t.Fatalf("Error building a clusterMap from new netbox data: %v\n", err)
	}

	if updatedClusterLen <= len(clusterMap) {
		t.Fatalf("Failed to delete cluster data")
	}
	if preUpdatedVMLen <= len(vmMap) {
		t.Fatalf("Failed to delete vm data")
	}
}

// simulates a vcenter with a cluster and a host
// Here are the resources I used to make this Frankenstein's monster
// https://github.com/vmware/govmomi/blob/master/simulator/virtual_machine_test.go
// https://github.com/vmware/govmomi/blob/master/simulator/finder_test.go
// https://github.com/vmware/govmomi/blob/master/simulator/cluster_compute_resource_test.go
func simVcenter(ctx context.Context) (*govmomi.Client, *simulator.Server, error) {
	m := simulator.VPX()
	m.Folder = 1

	err := m.Create()
	if err != nil {
		return nil, nil, err
	}

	ts := m.Service.NewServer()

	c, err := govmomi.NewClient(ctx, ts.URL, true)
	if err != nil {
		return nil, nil, err
	}

	finder := find.NewFinder(c.Client, false)

	dc, err := finder.Datacenter(ctx, "/F0/DC0")
	if err != nil {
		return nil, nil, err
	}

	finder.SetDatacenter(dc)

	hostf, err := finder.Folder(ctx, dc.InventoryPath+"/host/F0")
	if err != nil {
		return nil, nil, err
	}

	folders, err := dc.Folders(ctx)
	if err != nil {
		return nil, nil, err
	}

	f, err := folders.HostFolder.CreateFolder(ctx, "MyHosts")
	if err != nil {
		return nil, nil, err
	}

	task, _ := f.MoveInto(ctx, []types.ManagedObjectReference{hostf.Reference()})
	if err = task.Wait(ctx); err != nil {
		return nil, nil, err
	}

	cluster, err := folders.HostFolder.CreateCluster(ctx, "cluster1", types.ClusterConfigSpecEx{})
	if err != nil {
		return nil, nil, err
	}

	// build simulator VM
	host, err := finder.HostSystemOrDefault(ctx, "/F0/DC0/host/MyHosts/F0/DC0_C0/DC0_C0_H0")
	if err != nil {
		return nil, nil, err
	}

	pool, err := host.ResourcePool(ctx)
	if err != nil {
		return nil, nil, err
	}
	vmSpec := types.VirtualMachineConfigSpec{
		Name:    "VirtualMachine1",
		GuestId: string(types.VirtualMachineGuestOsIdentifierOtherGuest),
	}

	vmFolder := folders.VmFolder

	_, err = vmFolder.CreateVM(ctx, vmSpec, pool, host)
	if err != nil {
		return nil, nil, err
	}

	// This causes a panic, but it doesn't seem needed? I don't know why, and frankly I'm too scared to ask
	// info, _ := vm.WaitForResult(ctx, nil)
	// if err != nil {
	// 	t, _ := json.Marshal(info)
	// 	fmt.Println("info: ", string(t))
	// 	fmt.Println("err: ", err)
	// 	return nil, nil, err
	// }

	// vm := object.NewVirtualMachine(c.Client, info.Result.(types.ManagedObjectReference))

	clusterSpec := types.HostConnectSpec{}
	clusterSpec.HostName = "localhost"

	clusterTask, err := cluster.AddHost(ctx, clusterSpec, true, nil, nil)
	if err != nil {
		return nil, nil, err
	}

	_, err = clusterTask.WaitForResult(ctx, nil)
	if err != nil {
		return nil, nil, err
	}
	return c, ts, nil
}

func fakeNetbox() *client.NetboxClient {
	mux := http.NewServeMux()

	mux.HandleFunc("/api/virtualization/clusters/", testClusterHandler)
	mux.HandleFunc("/api/virtualization/virtual-machines/", testVirtualMachineHandler)
	mux.HandleFunc("/api/dcim/devices/", testDeviceHandler)

	mockServer := httptest.NewServer(mux)
	servURL := mockServer.URL
	netboxClient := client.NewNetboxClient("", servURL)

	return netboxClient
}

func setupFakeNetbox() (*client.NetboxClient, error) {
	netboxClient := fakeNetbox()

	// Cluster 1 test data
	clustName1 := "TestClust1"
	typeName1 := "VMWare"
	groupName1 := "Company1"
	tenantName1 := "Dept1"
	devCount1 := 1.0
	vmCount1 := 1.0
	customField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}

	clusterData1 := client.Cluster{
		BaseObject: client.BaseObject{
			CustomFields: &customField1,
			Display:      &clustName1,
			Id:           105,
			Name:         clustName1,
		},
		DeviceCount: &devCount1,
		Type: &client.Reference{
			Id:   1,
			Name: typeName1,
			Slug: &typeName1,
		},
		Group: &client.Reference{
			Id:   1,
			Name: groupName1,
			Slug: &groupName1,
		},
		VirtualMachineCount: &vmCount1,
		Comments:            "",
		Site:                nil,
		Tenant: &client.Reference{
			Id:   1,
			Name: tenantName1,
			Slug: &tenantName1,
		},
	}

	// CreateClusters
	ClusterArr := []client.Cluster{clusterData1}

	err := netboxClient.CreateClusters(ClusterArr)
	if err != nil {
		return nil, err
	}

	VMName1 := "TestVM1"
	VMIPv4Label := "IPv4Label1"
	VMIPv4DNS := "IPv4DNS1"
	vmClustName1 := "Cluster1"
	siteName1 := "site1"
	platformName1 := "platform1"
	roleName1 := "role1"
	status1 := "status1"
	vmTenantName1 := "Dept1"
	IPv4IPCount1 := 1
	IPv4PrefixCount1 := 1
	vmCustomField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}

	VMData1 := client.VirtualMachine{
		BaseObject: client.BaseObject{
			CustomFields: &vmCustomField1,
			Display:      &VMName1,
			Id:           105,
			Name:         VMName1,
		},
		PrimaryIPv4: &client.IPv4{
			Address: "address1",
			Id:      1.0,
			Status: &client.Status{
				Label: &VMIPv4Label,
				Value: "IPv4Value1",
			},
			Url:     "IPv4Url1",
			DNSName: &VMIPv4DNS,
			VRF: &client.VRF{
				Id:   1.0,
				Url:  "IPv4VRFUrl1",
				Name: "IPv4VRF1",
				Tenant: &client.Reference{
					Id:   1.0,
					Name: tenantName1,
					Slug: &tenantName1,
				},
				IPCount:       &IPv4IPCount1,
				PrefixCount:   &IPv4PrefixCount1,
				EnforceUnique: true,
			},
		},
		Cluster: &client.Reference{
			Id:   1.0,
			Name: vmClustName1,
			Slug: &vmClustName1,
		},
		VCPUs:  1,
		Memory: 1,
		Disk:   1,
		Site: &client.Reference{
			Id:   1.0,
			Name: siteName1,
			Slug: &siteName1,
		},
		Platform: &client.Reference{
			Id:   1.0,
			Name: platformName1,
			Slug: &platformName1,
		},
		Role: &client.Reference{
			Id:   1.0,
			Name: roleName1,
			Slug: &roleName1,
		},
		Status: &client.Status{
			Label: &status1,
			Value: status1,
		},
		Tenant: &client.Reference{
			Id:   1,
			Name: vmTenantName1,
			Slug: &vmTenantName1,
		},
	}

	// Testing CreateVirtualMachines
	VirtualMachineArr := []client.VirtualMachine{VMData1}

	err = netboxClient.CreateVirtualMachines(VirtualMachineArr)
	if err != nil {
		return nil, err
	}

	// Device 1 test data
	deviceName1 := "TestDevice1"
	typeManufacturerName1 := "manufacturer1"
	typeModel1 := "model1"
	devRoleName1 := "deviceRole1"
	devTenantName1 := "Dept1"
	devPlatformName1 := "platform1"
	devSiteName1 := "site1"
	locationName1 := "location1"
	rackName1 := "rack1"
	position1 := 1.0
	face1 := "face1"
	parentDevice1 := "parent1"
	airflow1 := "airflow1"
	virtualChassisName1 := "chassis1"
	virtualChassisMasterName1 := "masterChassis1"
	deviceCustomField1 := map[string]interface{}{"vCenter Server": "test-vcenter1"}
	type context1 interface {
		context() string
	}
	var configContext1 context1

	DeviceData1 := client.Device{
		BaseObject: client.BaseObject{
			CustomFields: &deviceCustomField1,
			Display:      &deviceName1,
			Id:           105,
			Name:         deviceName1,
		},
		DeviceType: &client.DeviceType{
			Id: 1.0,
			Manufacturer: &client.Reference{
				Id:   1.0,
				Name: typeManufacturerName1,
				Slug: &typeManufacturerName1,
			},
			Model: typeModel1,
			Slug:  &typeModel1,
		},
		DeviceRole: &client.Reference{
			Id:   1.0,
			Name: devRoleName1,
			Slug: &devRoleName1,
		},
		Tenant: &client.Reference{
			Id:   1,
			Name: devTenantName1,
			Slug: &devTenantName1,
		},
		Platform: &client.Reference{
			Id:   1.0,
			Name: devPlatformName1,
			Slug: &devPlatformName1,
		},
		Serial:   "serial1",
		AssetTag: "asset1",
		Site: &client.Reference{
			Id:   1.0,
			Name: devSiteName1,
			Slug: &devSiteName1,
		},
		Location: &client.Reference{
			Id:   1.0,
			Name: locationName1,
			Slug: &locationName1,
		},
		Rack: &client.Reference{
			Id:   1.0,
			Name: rackName1,
			Slug: &rackName1,
		},
		Position: position1,
		Face: &client.Status{
			Label: &face1,
			Value: face1,
		},
		ParentDevice: &client.Reference{
			Id:   1.0,
			Name: parentDevice1,
			Slug: &parentDevice1,
		},
		Airflow: &client.Status{
			Label: &airflow1,
			Value: airflow1,
		},
		PrimaryIP: &client.IP{
			Id: 1.0,
		},
		PrimaryIPv6: &client.IP{
			Id: 1.0,
		},
		Cluster: &client.DeviceCluster{
			Id:   1.0,
			Name: "Cluster1",
		},
		VirtualChassis: &client.VirtualChassis{
			Id:      1.0,
			Url:     "VirtualChassisUrl1",
			Name:    virtualChassisName1,
			Display: &virtualChassisName1,
			Master: &client.Descriptor{
				Id:      1.0,
				Url:     "VirtualChassisMasterUrl1",
				Display: &virtualChassisMasterName1,
				Name:    virtualChassisMasterName1,
			},
		},
		VCPosition:       1.0,
		VCPriority:       1.0,
		Comments:         "comment1",
		LocalContextData: "localContext1",
		ConfigContext:    configContext1,
	}

	// Testing CreateDevices
	DeviceArr := []client.Device{DeviceData1}

	err = netboxClient.CreateDevices(DeviceArr)
	if err != nil {
		return nil, err
	}

	return netboxClient, nil
}

type GenericStructs interface {
	client.Cluster | client.Device | client.VirtualMachine
}

func handlerGeneric[T GenericStructs](w http.ResponseWriter, r *http.Request, JSONDataArr client.NetboxJSONData, PrevArrStorage []T, ArrStorage []T) ([]byte, []T, []T, *client.NetboxJSONData) {
	if r.Method == "POST" || r.Method == "PATCH" {
		prevRaw, err := json.Marshal(JSONDataArr.Results)
		if err != nil {
			log.Fatalf("Error marshaling old interface data: %v\n", err)
		}

		// grabs write data
		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			log.Fatalf("Problem reading request: %v\n", err)
		}

		// pushes write data into array of Clusters
		err = json.Unmarshal([]byte(reqBody), &ArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling Cluster data: %v\n", err)
		}

		err = json.Unmarshal([]byte(prevRaw), &PrevArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling previous data %v\n", err)
		}

		ArrStorage = append(ArrStorage, PrevArrStorage...)

		// build proper interface to store in json file
		jsonData := client.NetboxJSONData{
			Next:    nil,
			Results: ArrStorage,
		}

		return []byte(reqBody), nil, nil, &jsonData
	} else if r.Method == "GET" {
		readData, err := json.Marshal(JSONDataArr)
		if err != nil {
			log.Fatalf("JSONDataArr failed to marshal: %v\n", err)
		}

		return []byte(readData), nil, nil, nil

	} else if r.Method == "DELETE" {
		prevRaw, err := json.Marshal(JSONDataArr.Results)
		if err != nil {
			log.Fatalf("Error marshaling old interface data: %v\n", err)
		}

		reqBody, err := io.ReadAll(r.Body)
		if err != nil {
			log.Fatalf("Error reading delete data: %v\n", err)
		}

		err = json.Unmarshal([]byte(reqBody), &ArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling delete data: %v\n", err)
		}
		err = json.Unmarshal([]byte(prevRaw), &PrevArrStorage)
		if err != nil {
			log.Fatalf("Error unmarshaling previous data %v\n", err)
		}

		return []byte(reqBody), ArrStorage, PrevArrStorage, nil
	}
	return nil, nil, nil, nil
}

func testClusterHandler(w http.ResponseWriter, r *http.Request) {
	ArrStorage := []client.Cluster{}
	PrevArrStorage := []client.Cluster{}
	writeData, ArrStorage, PrevArrStorage, jsonData := handlerGeneric(w, r, JSONClusterDataArr, PrevArrStorage, ArrStorage)
	if r.Method == "POST" || r.Method == "PATCH" {
		// write cluster data to request and update JSONClusterDataArr with new values
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
		JSONClusterDataArr = *jsonData
	} else if r.Method == "GET" {
		// write cluster data to request
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
	} else if r.Method == "DELETE" {
		for index, prevValue := range PrevArrStorage {
			for _, delValue := range ArrStorage {
				if prevValue.Name == delValue.Name {
					temp := PrevArrStorage[index+1:]
					temp = append(temp, PrevArrStorage[:index]...)
					PrevArrStorage = temp
				}
			}
		}
		jsonData := client.NetboxJSONData{
			Next:    nil,
			Results: PrevArrStorage,
		}

		// Write NetboxJSONData to file and write cluster data to request
		JSONClusterDataArr = jsonData

		_, err := w.Write([]byte(writeData))
		if err != nil {
			log.Fatalf("Failed to write delete data: %v\n", err)
		}
	}
}

func testVirtualMachineHandler(w http.ResponseWriter, r *http.Request) {
	ArrStorage := []client.VirtualMachine{}
	PrevArrStorage := []client.VirtualMachine{}
	writeData, ArrStorage, PrevArrStorage, jsonData := handlerGeneric(w, r, JSONVMDataArr, PrevArrStorage, ArrStorage)
	if r.Method == "POST" || r.Method == "PATCH" {
		// write virtual machine data to request and update JSONVMDataArr with new values
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
		JSONVMDataArr = *jsonData
	} else if r.Method == "GET" {
		// write virtual machine data to request
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
	} else if r.Method == "DELETE" {
		for index, prevValue := range PrevArrStorage {
			for _, delValue := range ArrStorage {
				if prevValue.Name == delValue.Name {
					temp := PrevArrStorage[index+1:]
					temp = append(temp, PrevArrStorage[:index]...)
					PrevArrStorage = temp
				}
			}
		}
		jsonData := client.NetboxJSONData{
			Next:    nil,
			Results: PrevArrStorage,
		}

		// Write NetboxJSONData to file and write virtual machine data to request
		JSONVMDataArr = jsonData

		_, err := w.Write([]byte(writeData))
		if err != nil {
			log.Fatalf("Failed to write delete data: %v\n", err)
		}
	}
}

func testDeviceHandler(w http.ResponseWriter, r *http.Request) {
	ArrStorage := []client.Device{}
	PrevArrStorage := []client.Device{}
	writeData, ArrStorage, PrevArrStorage, jsonData := handlerGeneric(w, r, JSONDeviceDataArr, PrevArrStorage, ArrStorage)
	if r.Method == "POST" || r.Method == "PATCH" {
		// write device data to request and cd JSONDeviceDataArr with new values
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
		JSONDeviceDataArr = *jsonData
	} else if r.Method == "GET" {
		// write device data to request
		_, err := w.Write(writeData)
		if err != nil {
			log.Fatalf("Failed to write all cluster data: %v\n", err)
		}
	} else if r.Method == "DELETE" {
		for index, prevValue := range PrevArrStorage {
			for _, delValue := range ArrStorage {
				if prevValue.Name == delValue.Name {
					temp := PrevArrStorage[index+1:]
					temp = append(temp, PrevArrStorage[:index]...)
					PrevArrStorage = temp
				}
			}
		}
		jsonData := client.NetboxJSONData{
			Next:    nil,
			Results: PrevArrStorage,
		}

		// Write NetboxJSONData to file and write device data to request
		JSONDeviceDataArr = jsonData

		_, err := w.Write([]byte(writeData))
		if err != nil {
			log.Fatalf("Failed to write delete data: %v\n", err)
		}
	}
}

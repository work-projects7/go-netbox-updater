package netbox

import (
	"log"
	"strings"

	"github.com/vmware/govmomi/vim25/mo"
	"gitlab.com/work-projects7/go-netbox-updater/config"
	"gitlab.com/work-projects7/go-netbox/client"
	govscanner "gitlab.com/work-projects7/go-vscanner"
)

const defaultLimit = 50

/*
Cluster is the base level of object inside of Netbox.
The Site and BaseObject are the only pieces dynamically generated.
Rest are pulled through a .yaml because Group, Tenant, and Type are not unique per cluster
*/
func UpdateNetboxClusters(vCenterServerName string, clusterMapPtr *map[string]client.Cluster, deleteClusterMap map[string]client.Cluster, vCenterClient *govscanner.VMWareClient, netboxClient *client.NetboxClient, configData config.Config) error {
	//check if cluster exists in netbox
	vCenterClusters, err := vCenterClient.GetMoClusterCompute(nil)
	if err != nil {
		return err
	}

	createClusterList := []client.Cluster{}
	updateClusterList := []client.Cluster{}
	clusterMap := *clusterMapPtr
	for _, cluster := range vCenterClusters {
		clusterName := cluster.Name
		siteRef := client.Reference{}
		hostRef := mo.HostSystem{}

		if len(cluster.ComputeResource.Host) != 0 {
			// Finds which site the cluster is a part of by grabbing the first available Host and using it as a reference to the Site
			err := vCenterClient.GetManagedObjectReferenceAttributes(&cluster.Host[0], []string{}, &hostRef)
			if err != nil {
				return err
			}
			siteRef, err = getSiteFromHostRef(&hostRef, configData, vCenterClient)
			if err != nil {
				return err
			}
		} else {
			continue
		}
		//make cluster object locally.
		netboxClusterEntry := &client.Cluster{
			BaseObject: client.BaseObject{
				CustomFields: &map[string]interface{}{
					configData.VCenterCustomField: vCenterServerName,
				},
				Display: &clusterName,
				Name:    clusterName,
				Tags:    []interface{}{},
			},
			Group:  &configData.DefaultGroup,
			Type:   &configData.DefaultType,
			Site:   &siteRef,
			Tenant: &configData.Tenant,
		}

		log.Printf("Checking if %v cluster exists in netbox...", clusterName)
		if value, ok := clusterMap[clusterName]; ok {
			log.Printf("Cluster: %v already exists in netbox, adding to update queue.", clusterName)
			netboxClusterEntry.Group = &configData.DefaultGroup
			netboxClusterEntry.Id = value.BaseObject.Id
			netboxClusterEntry.CustomFields = value.BaseObject.CustomFields
			(*netboxClusterEntry.CustomFields)[configData.VCenterCustomField] = vCenterServerName
			updateClusterList = append(updateClusterList, *netboxClusterEntry)
			/* Deleting Clusters as they are found in Netbox and Vcenter all that
			will remain are Clusters not in Vcenter, but are in Netbox.
			An exact copy of clusterMap is needed since clusterMap is used for later structs
			and Clusters can't be removed from the map preemptively */
			delete(deleteClusterMap, clusterName)
		} else if !ClusterContains(createClusterList, *netboxClusterEntry) {
			// !ClusterContains is used to ensure duplicate Clusters don't get added twice
			log.Printf("Cluster: %v didn't exist in netbox, adding to creation queue.", clusterName)
			createClusterList = append(createClusterList, *netboxClusterEntry)
		} else {
			log.Printf(" Cluster: %v had a problem and was not added to creation or update queue.", netboxClusterEntry.Name)
		}
	}

	if len(createClusterList) > 0 {
		log.Printf("Creating %v new Cluster entries\n", len(createClusterList))
		err := netboxClient.CreateClusters(createClusterList)
		if err != nil {
			return err
		}

		/* Since Cluster needs to be accurate for Virtual Machine structs
		update clusterMap with newly created Clusters*/
		clusterMap, _, err = MakeNetboxClusterMap(netboxClient)
		if err != nil {
			return err
		}
		*clusterMapPtr = clusterMap

	}
	if len(updateClusterList) > 0 {
		log.Printf("Updating %v Cluster entries\n", len(updateClusterList))
		err = netboxClient.UpdateClusters(updateClusterList)
		if err != nil {
			return err
		}
	}

	return nil
}

func ClusterContains(clusterList []client.Cluster, target client.Cluster) bool {
	for _, cluster := range clusterList {
		if cluster.Name == target.Name {
			return true
		}
	}
	return false
}

func MakeNetboxClusterMap(netboxClient *client.NetboxClient) (map[string]client.Cluster, map[string]client.Cluster, error) {
	/* Building two clusterMaps, one for Devices and Virtual Machines to reference
	and one to keep track of Clusters no longer in VCenter */
	clusterMap := map[string]client.Cluster{}
	deleteClusterMap := map[string]client.Cluster{}
	netboxClusters, err := netboxClient.GetClusters(defaultLimit)
	if err != nil {
		return nil, nil, err
	}

	for _, cluster := range netboxClusters {
		clusterMap[cluster.Name] = cluster
		deleteClusterMap[cluster.Name] = cluster
	}

	return clusterMap, deleteClusterMap, nil
}

func MakeNetboxVirtualMachineMap(netboxClient *client.NetboxClient) (map[string]client.VirtualMachine, error) {
	vmMap := map[string]client.VirtualMachine{}
	netboxVMs, err := netboxClient.GetVirtualMachines(defaultLimit)
	if err != nil {
		return nil, err
	}

	for _, vm := range netboxVMs {
		vmMap[vm.Name] = vm
	}

	return vmMap, nil
}

// Unused. May have use later
func MakeNetboxVRFMap(netboxClient *client.NetboxClient) (map[string]client.VRF, error) {
	vrfMap := map[string]client.VRF{}
	netboxVRFs, err := netboxClient.GetVRFs(defaultLimit)
	if err != nil {
		return nil, err
	}

	for _, vrf := range netboxVRFs {
		vrfMap[vrf.Name] = vrf
	}

	return vrfMap, nil
}

func MakeNetboxDeviceMap(netboxClient *client.NetboxClient) (map[string]client.Device, error) {
	deviceMap := map[string]client.Device{}
	netboxDevices, err := netboxClient.GetDevices(defaultLimit)
	if err != nil {
		return nil, err
	}

	for _, device := range netboxDevices {
		deviceMap[device.BaseObject.Name] = device
	}
	return deviceMap, nil
}

func UpdateNetboxVirtualMachines(vmMap map[string]client.VirtualMachine, clusterMap map[string]client.Cluster, deviceMap map[string]client.Device, missingHostMap map[string][]string, vCenterClient *govscanner.VMWareClient, netboxClient *client.NetboxClient, configData config.Config) (map[string][]string, error) {
	netboxUpdateVMList := []client.VirtualMachine{}
	netboxCreateVMList := []client.VirtualMachine{}
	missingHostList := []string{}

	vCenterVMs, err := vCenterClient.GetMoVirtualMachines([]string{"name", "summary"})
	if err != nil {
		return nil, err
	}

	for _, vCenterVM := range vCenterVMs {
		/* Virtual Machines are almost entirely dynamically generated based on the data
		in VCenter.
		*/
		vmName := vCenterVM.Name
		hostRef := &mo.HostSystem{}
		var clusterRef *client.Reference
		host := vCenterVM.Summary.Runtime.Host

		// It skips Templates because there are same named Templates in different VCenters.
		if vCenterVM.Summary.Config.Template {
			continue
		}

		// VMs will only not have a host, if they are presently being migrated
		if host == nil {
			continue
		}

		err := vCenterClient.GetManagedObjectReferenceAttributes(host, []string{}, hostRef)
		if err != nil {
			return nil, err
		}

		// No Cluster is a custom Cluster built in netbox for VMs with no Cluster
		vCenterClusterName, err := vCenterClient.GetVirtualMachineClusterName(&vCenterVM)
		if err != nil {
			clusterRef = getClusterReferenceFromName(clusterMap, "No Cluster")
		} else {
			clusterRef = getClusterReferenceFromName(clusterMap, vCenterClusterName)
		}

		siteRef, err := getSiteFromHostRef(hostRef, configData, vCenterClient)
		if err != nil {
			return nil, err
		}

		// Removing the DNS to get only the host name
		parsedHost := strings.Split(hostRef.Name, ".")

		netboxVMEntry := &client.VirtualMachine{
			BaseObject: client.BaseObject{
				CustomFields: &map[string]interface{}{},
				Display:      &vmName,
				Name:         vCenterVM.Name,
				Tags:         []interface{}{},
			},
			Tenant:  &configData.Tenant,
			Cluster: clusterRef,
			Site:    &siteRef,
			Role:    &configData.VMRole,
		}

		/* If a host doesn't exist in netbox, it can't be attached to the VM.
		The VirtualMachine struct does not omitempty Device because it caused
		the old Device to be attached, which could be in a different Cluster.
		*/
		if deviceObject, ok := deviceMap[parsedHost[0]]; ok {
			netboxVMEntry = &client.VirtualMachine{
				BaseObject: client.BaseObject{
					CustomFields: &map[string]interface{}{},
					Display:      &vmName,
					Name:         vCenterVM.Name,
					Tags:         []interface{}{},
				},
				Tenant:  &configData.Tenant,
				Cluster: clusterRef,
				Site:    &siteRef,
				Device: &client.Reference{
					Name: deviceObject.BaseObject.Name,
				},
				Role: &configData.VMRole,
			}
		} else {
			// TODO: Turn the missingHostList into an email
			dupl := true
			for _, host := range missingHostList {
				if host == parsedHost[0] {
					dupl = false
				}
			}
			if dupl {
				missingHostList = append(missingHostList, parsedHost[0])
			}
		}
		// Turns the missingHostList into a map, under the VCenter name
		missingHostMap[vCenterClient.VcenterName] = missingHostList

		//Check vm map for matching entry, if exists add to update, else add to create.
		log.Printf("Checking if %v vm exists in netbox...", vmName)
		if value, ok := vmMap[vmName]; ok && vmMap[vmName].Name != "" {
			log.Printf("VM: %v already exists in netbox, adding to update queue.", vmName)
			netboxVMEntry.Id = value.BaseObject.Id
			netboxUpdateVMList = append(netboxUpdateVMList, *netboxVMEntry)
			/* After a VM has been found, it changes the name to "" instead of deleting it
			from the map. This is because duplicate VMs can exist and will cause a conflict.
			The if statements have a check to make sure the vmName is "" to avoid duplicates
			*/
			usedVM := client.VirtualMachine{}
			vmMap[vmName] = usedVM
		} else if len(netboxVMEntry.Name) <= 64 && (vmMap[vmName].Name != "" || !ok) {
			log.Printf("VM: %v didn't exist in netbox, adding to creation queue.", vmName)
			//VM didn't exist, so POST to make one.
			usedVM := client.VirtualMachine{}
			vmMap[vmName] = usedVM
			netboxCreateVMList = append(netboxCreateVMList, *netboxVMEntry)
		} else {
			// This catches the duplicate VM and skips it
			log.Printf(" Cluster: %v had a problem and was not added to creation or update queue.", netboxVMEntry.Name)
			continue
		}
	}

	log.Printf("Creating %v new VM Entries\n", len(netboxCreateVMList))
	err = netboxClient.CreateVirtualMachines(netboxCreateVMList)
	if err != nil {
		return nil, err
	}
	log.Printf("Updating %v VM Entries\n", len(netboxUpdateVMList))
	err = netboxClient.UpdateVirtualMachines(netboxUpdateVMList)
	if err != nil {
		return nil, err
	}

	return missingHostMap, nil
}

/*
Devices are the most intricate and important struct in netbox.
Because of the complexity it is impossible to dynamically create Devices.
This function will only update the Cluster of Devices, to ensure the Devices are
in sync with Virtual Machines
*/
func UpdateNetboxDeviceClusters(deviceMap map[string]client.Device, clusterMap map[string]client.Cluster, deleteClusterMap map[string]client.Cluster, vCenterClient *govscanner.VMWareClient, netboxClient *client.NetboxClient) error {
	netboxUpdateDeviceClusterList := []client.Device{}

	vCenterDeviceClusterArr, err := vCenterClient.GetHostClusterNameArr([]string{})
	if err != nil {
		return err
	}

	for _, VCenterDevice := range vCenterDeviceClusterArr {
		clustName := VCenterDevice.ClusterName
		// Recycles all of the data in Netbox, except for Cluster
		if value, ok := deviceMap[VCenterDevice.HostName]; ok {
			baseObj := &client.BaseObject{
				CustomFields: value.BaseObject.CustomFields,
				Display:      value.BaseObject.Display,
				Id:           value.BaseObject.Id,
				Name:         value.BaseObject.Name,
				Tags:         value.BaseObject.Tags,
				URL:          value.BaseObject.URL,
			}
			netboxEntry := &client.DeviceCluster{
				Name: clustName,
				Id:   clusterMap[clustName].BaseObject.Id,
			}
			value.BaseObject = *baseObj
			value.Cluster = netboxEntry
			value.Face = nil

			log.Printf("Adding %v to device update queue...\n", value.BaseObject.Name)
			netboxUpdateDeviceClusterList = append(netboxUpdateDeviceClusterList, value)
		}
	}

	log.Printf("Updating %v Device Entries\n", len(netboxUpdateDeviceClusterList))

	err = netboxClient.UpdateDevices(netboxUpdateDeviceClusterList)
	if err != nil {
		return err
	}

	return nil
}

func getClusterReferenceFromName(clusterMap map[string]client.Cluster, clusterName string) *client.Reference {
	if value, ok := clusterMap[clusterName]; ok {
		reference := &client.Reference{
			Id:   value.BaseObject.Id,
			Name: value.BaseObject.Name,
			Slug: nil,
		}
		return reference
	}
	return nil
}

func getSiteFromHostRef(hostRef *mo.HostSystem, configData config.Config, vCenterClient *govscanner.VMWareClient) (client.Reference, error) {
	// Find a host relating to the VM in order to pull a site
	siteRef := client.Reference{}

	siteTag := string(hostRef.ManagedEntity.Name[:3])

	if siteRefTemp, ok := configData.Sites[siteTag]; ok {
		siteRef = siteRefTemp
	}
	return siteRef, nil
}

package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/vmware/govmomi"
	"github.com/vmware/govmomi/view"
	"gitlab.com/work-projects7/go-netbox-updater/config"
	"gitlab.com/work-projects7/go-netbox-updater/netbox"
	client "gitlab.com/work-projects7/go-netbox/client"
	govscanner "gitlab.com/work-projects7/go-vscanner"
	"gopkg.in/yaml.v2"
)

var configData config.Config

func main() {

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true} // Don't know what this line is attempting to do. Is it firewall settings?
	configPath := os.Args[1:]                                                                       // Grabs the command line arguments, skipping the program name
	loadYAML(&configData, configPath[0])                                                            // Runs the loadYAML func with params configData and configPath location
	connectionList := loadConnections(configData.VcenterInfo)
	netboxClient := client.NewNetboxClient(configData.ApiToken, configData.ApiAddress)
	clusterMap, deleteClusterMap, vmMap, _, deviceMap, err := makeNetboxMaps(netboxClient) // TODO: vrfMap, siteMap
	if err != nil {
		standardError(err)
	}

	netboxDeleteVMList := []client.VirtualMachine{}
	netboxDeleteClusterList := []client.Cluster{}
	netboxDeleteDeviceList := []client.Device{}
	missingHostMap := map[string][]string{}

	// setting up connection to list of vcenters and pushing data to netbox
	for _, connection := range connectionList {
		defer connection.Cancel() // closes connection once data is pushed to netbox
		fmt.Println("Checking " + connection.VcenterName)
		err = netbox.UpdateNetboxClusters(connection.VcenterName, &clusterMap, deleteClusterMap, connection, netboxClient, configData) // Grabs all the cluster info of the vcenters and updates the netbox data entry if data has been changed or creates a new cluster if it didn't exist
		if err != nil {                                                                                                                // Error checking to ensure netbox data updated properly
			fmt.Printf("ERROR: %v\n", err)
		}
		err = netbox.UpdateNetboxDeviceClusters(deviceMap, clusterMap, deleteClusterMap, connection, netboxClient)
		if err != nil {
			standardError(err)
		}
		missingHostMap, err = netbox.UpdateNetboxVirtualMachines(vmMap, clusterMap, deviceMap, missingHostMap, connection, netboxClient, configData) // Pulls all the vcenter VM data and updates the netbox data entry if data has changed, or creates a new VM data entry if it didn't exist
		if err != nil {                                                                                                                              // Pushes an error to standard error if updating/creating VM data entries failed
			standardError(err)
		}

	}
	fmt.Println("All runs completed.")

	// TODO: turn this loop into email, for informing on missing devices in netbox
	// for vcenter, host := range missingHostMap {
	// 	fmt.Println("Vcenter: ", vcenter, ", Devices: ", host)
	// }

	for _, value := range vmMap {
		if value.Name != "" {
			netboxDeleteVMList = append(netboxDeleteVMList, value)
		}
	}
	for _, value := range deleteClusterMap {
		if value.Name != "No Cluster" {
			netboxDeleteClusterList = append(netboxDeleteClusterList, value)
		}
	}
	/* In order to delete Clusters, they can't have VMs or Devices attached.
	If a Cluster is to be deleted, all relevant Devices will be migrated to a
	new Cluster. Any remaining Clusters are assumed to be no longer in use and deleted.
	*/
	for _, device := range deviceMap {
		if device.Cluster != nil {
			if _, ok := deleteClusterMap[device.Cluster.Name]; ok {
				netboxDeleteDeviceList = append(netboxDeleteDeviceList, device)
			}
		}
	}

	log.Printf("Deleting %v VM Entries\n", len(netboxDeleteVMList))
	err = netboxClient.DeleteVirtualMachines(netboxDeleteVMList)
	if err != nil {
		standardError(err)
	}

	log.Printf("Deleting %v Device Entries\n", len(netboxDeleteDeviceList))
	err = netboxClient.DeleteDevices(netboxDeleteDeviceList)
	if err != nil {
		standardError(err)
	}
	log.Printf("Deleting %v Cluster Entries\n", len(netboxDeleteClusterList))
	err = netboxClient.DeleteClusters(netboxDeleteClusterList)
	if err != nil {
		standardError(err)
	}

}

func standardError(err error) {
	fmt.Printf("ERROR: %v\n", err) // prints error
	log.Printf("ERROR: %v\n", err) // pushes errors to log
}

func makeNetboxMaps(netboxClient *client.NetboxClient) (map[string]client.Cluster, map[string]client.Cluster, map[string]client.VirtualMachine, map[string]*client.VRF, map[string]client.Device, error) {
	clusterMap, deleteClusterMap, err := netbox.MakeNetboxClusterMap(netboxClient) // returns a map of cluster names that points to the corresponding netboxClient.Clusters struct
	if err != nil {                                                                // error checking to ensure the map contains the correct info. returns nil except for the error
		return nil, nil, nil, nil, nil, err
	}
	vmMap, err := netbox.MakeNetboxVirtualMachineMap(netboxClient) // returns a map of VM names that points to the corresponding aVM data
	if err != nil {                                                // returns an error if the map failed to build or contains bad data
		return nil, nil, nil, nil, nil, err
	}
	// vrfMap, err := netbox.MakeNetboxVRFMap(netboxClient) // returns a map of VRF names that points to the corresponding VRF data
	// if err != nil {                                      // returns an error if the map failed to build or contains bad data
	// 	return nil, nil, nil, nil, err
	// }
	DeviceMap, err := netbox.MakeNetboxDeviceMap(netboxClient) // returns a map of Site names that points to the corresponding Site data
	if err != nil {                                            //returns an error if the map failed to build or contains bad data
		return nil, nil, nil, nil, nil, err
	}
	return clusterMap, deleteClusterMap, vmMap, nil, DeviceMap, nil
}

// function that ... and returns a pointer to a slice of VMWareClient structs
func loadConnections(vCenters map[string]config.VCenter) []*govscanner.VMWareClient {
	connectionList := make([]*govscanner.VMWareClient, 0)
	sortedKeys := []string{}

	for key := range vCenters {
		sortedKeys = append(sortedKeys, key)
	}

	for _, sortK := range sortedKeys {
		vCenterContext, cancel := context.WithCancel(context.Background())
		directURL, err := url.Parse("https://" + url.QueryEscape(vCenters[sortK].User) + ":" + url.QueryEscape(vCenters[sortK].Password) + "@" + sortK + "/sdk") // parses a url to access the vcenter with proper login credentials
		if err != nil {
			fmt.Printf("Error parsing url: %s\n", err.Error())
			log.Fatalf("Error parsing url: %s\n", err.Error())
		}

		govmomiClient, err := govmomi.NewClient(vCenterContext, directURL, true)
		if err != nil {
			fmt.Println(sortK)
			fmt.Printf("Logging in error: %s\n", err.Error())
			log.Fatalf("Logging in error: %s\n", err.Error())
		}

		Client := govscanner.NewVMWareClient(vCenterContext, sortK, govmomiClient, view.NewManager(govmomiClient.Client))
		Client.Cancel = cancel
		connectionList = append(connectionList, Client)
	}
	return connectionList

}

// a func that reads in a path to a config file (configPath), decodes it data to configData and preps a log file for output
func loadYAML(configData *config.Config, configPath string) {
	currentTime := time.Now() // gets current date and time, for accurate log

	yamlFile, err := os.ReadFile(configPath) // ReadFile has two outputs, the contents of the config file and an err. If working err == nil
	if err != nil {                          // error handling, if YAML fails to read
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &configData) // decodes the YAML file and pushes the info to the configData pointer
	if err != nil {                             // error checking if YAML file failed to be decoded
		log.Fatalf("Unmarshal: %v", err)
	}
	file, err := os.OpenFile(configData.LogPath+currentTime.Format("2006-01-02")+".log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666) // appends/creates/writes log file named after the currentTime, I don't understand the 0666
	if err != nil {                                                                                                                 // error checking if log file fails
		log.Fatal(err)
	}
	log.SetOutput(file) // writes log. outputs to designated log file
}
